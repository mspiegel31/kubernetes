# Installation

## Table of Contents

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Installation](#installation)
  - [Table of Contents](#table-of-contents)
  - [Server Installation](#server-installation)
  - [Host Configuration and Kubernetes Cluster installation](#host-configuration-and-kubernetes-cluster-installation)
  - [Login with username and password](#login-with-username-and-password)
  - [Create dashboard user and retrieve access token](#create-dashboard-user-and-retrieve-access-token)

<!-- /code_chunk_output -->

## Server Installation

1. Install CentOS 7 DVD from USB
2. Configure a user for ansible access
3. Assign static IP addresses
4. Optionally add hosts to DNS

## Host Configuration and Kubernetes Cluster installation

1. Download kubespray and follow get-started guide (including copying the sample inventory objects into ./inventory/Justin-Tech/)
2. Copy hosts.ini into the folder created in the previous step
3. Rename the hosts and/or add IP address information as needed
4. Disable firewall or add port exceptions (specifically for etcd)
5. Run `ansible-playbook -i inventory/Justin-Tech/hosts.ini cluster.yml -b` from the route of the kubespray directory
   * [hosts.ini](../../src/installation/hosts.ini) as example

**NOTE:** [all.yml](../../src/installation/inventory/Justin-Tech/group_vars/all/all.yml) sets up DNS forward servers for dnsmasq, which may need to be changed

## Login with username and password

If you copy [k8s-cluster.yml](../../src/installation/inventory/Justin-Tech/group_vars/k8s-cluster/k8s-cluster.yml) to ./inventory/Justin-Tech/group_vars/k8s-cluster/ you will have basic auth enabled on the dashboard

1. Retrieve username and password from ./inventory/Justin-Tech/credentials/kube_user.creds
2. Username should be "admin"
3. Login to the dashboard

  * the dashboard can be found here: [https://first_master:6443/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login](https://first_master:6443/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login)

## Create dashboard user and retrieve access token

**NOTE:** This step is not needed if copying [k8s-cluster.yml](../../src/installation/inventory/Justin-Tech/group_vars/k8s-cluster/k8s-cluster.yml)

1. Follow instructions [here](https://github.com/kubernetes/dashboard/wiki/Creating-sample-user) to create a new user (see [new-user.yml](../../src/installation/new-user.yml))

   * `kubectl apply -f new-user.yml`
2. Get the dashboard user's token

   * `kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')`
3. Use the token from the output of the above command to login to the dashboard

  * the dashboard can be found here: [https://first_master:6443/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login](https://first_master:6443/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login)